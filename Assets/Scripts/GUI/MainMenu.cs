﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenu : MonoBehaviour {
	public Slider[] volumes;
	public Text[] percentText;
	public Transform[] mutes;
	public Image[] buttons;
	public Sprite grayButton;
	public Sprite whiteButton;

	private bool options;
	private Color pixelColor;
	private bool fxMute;
	private bool musicMute;
	private float fxVolume = 1f;
	private float musicVolume = 1f;
	private int selectColor;
	
	void Start () {
		if (!PlayerPrefs.HasKey("fxVolume")){
			PlayerPrefs.SetFloat("fxVolume",1f);
			PlayerPrefs.Save();
		}
		if (!PlayerPrefs.HasKey("musicVolume")){
			PlayerPrefs.SetFloat("musicVolume",1f);
			PlayerPrefs.Save();
		}
	}

	void StartGame () {
		Application.LoadLevel(1);
	}
	void QuitGame () {
		Application.Quit();
	}

	void Update () {
		//FxSound
		fxVolume = volumes[0].value;
		percentText[0].text = Mathf.RoundToInt(fxVolume*100)+"%";
		//MusicSound
		musicVolume = volumes[1].value;
		percentText[1].text = Mathf.RoundToInt(musicVolume*100)+"%";
	}

	void Options () {
		//Mutes en el que estaban
		fxMute = (PlayerPrefs.GetInt("fxMute")) == 1 ? true : false;
		musicMute = (PlayerPrefs.GetInt("musicMute")) == 1 ? true : false;
		mutes[0].gameObject.SetActive(!fxMute);
		mutes[1].gameObject.SetActive(fxMute);
		mutes[2].gameObject.SetActive(!musicMute);
		mutes[3].gameObject.SetActive(musicMute);
		//Volumenes en el que estaban
		volumes[0].value = PlayerPrefs.GetFloat("fxVolume");
		volumes[1].value = PlayerPrefs.GetFloat("musicVolume");
		//Color en el que estaba
		selectColor = PlayerPrefs.GetInt("colorInt");
		ChangeColor();
	}

	void SaveOptions () {
		PlayerPrefs.SetFloat("fxVolume",volumes[0].value);
		PlayerPrefs.SetInt("fxMute", fxMute ? 1 : 0);
		PlayerPrefs.SetFloat("musicVolume",volumes[1].value);
		PlayerPrefs.SetInt("musicMute", musicMute ? 1 : 0);
		PlayerPrefs.SetInt("colorInt",selectColor);
	}

	void FxMute () {
		fxMute = true;
	}
	void FxUnmute () {
		fxMute = false;
	}
	void MusicMute () {
		musicMute = true;
	}
	void MusicUnmute () {
		musicMute = false;
	}

	void Facebook () {
		Application.OpenURL("https://www.facebook.com/ThePixelGame?ref=ts&fref=ts");
	}
	void Twitter () {
		Application.OpenURL("https://twitter.com/tomas_rinaldi");
	}


	void SelectWhite () {
		selectColor = 0;
	}
	void SelectYellow () {
		selectColor = 1;
	}
	void SelectBlue () {
		selectColor = 2;
	}
	void SelectRed () {
		selectColor = 3;
	}
	void SelectGreen () {
		selectColor = 4;
	}
	void SelectPink () {
		selectColor = 5;
	}
	void SelectLBlue () {
		selectColor = 6;
	}
	void SelectOrange () {
		selectColor = 7;
	}
	void ChangeColor () {
		for(int i = 0; i < buttons.Length; i++){
			if (i == selectColor){
				buttons[i].sprite = whiteButton;
			}
			else {
				buttons[i].sprite = grayButton;
			}
		}
	}
}
