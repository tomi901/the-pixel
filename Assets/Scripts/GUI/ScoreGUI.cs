﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreGUI : MonoBehaviour {
	public int score;
	public Text scoreText;
	public float loseScale;

	private Color playingColor;
	private bool fadeColor = false;
	private float timer;
	private float startPos;

	void AddScore () {
		score ++;
		scoreText.text = score.ToString();
	}

	void Start () {
		playingColor = scoreText.color;
		startPos = transform.position.y;
	}

	void Update () {
		if (fadeColor) {
			scoreText.color = Color.Lerp(playingColor,Color.white,Time.timeSinceLevelLoad-timer);
			transform.localScale = Vector3.Lerp(Vector3.one,new Vector3(loseScale,loseScale,loseScale),Time.timeSinceLevelLoad-timer);
			transform.position = new Vector3(transform.position.x,Mathf.Lerp (startPos,startPos+loseScale*10,Time.timeSinceLevelLoad-timer),0f);
		}

		GameObject.FindGameObjectWithTag("Scene").SendMessage("Score",score);
	}

	void Lose () {
		fadeColor = true;

		timer = Time.timeSinceLevelLoad;
	}
}