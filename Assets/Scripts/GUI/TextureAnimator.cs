﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextureAnimator : MonoBehaviour {
	public Sprite[] frames;
	public float framesPerSecond;
	
	void Update () {
		int index = (int) (Time.timeSinceLevelLoad * framesPerSecond) % frames.Length;
		gameObject.GetComponent<Image>().sprite = frames[index];
	}
}
