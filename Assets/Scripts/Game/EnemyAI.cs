﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {
	public Transform explosionPrefab;
	public AudioClip enemyExplSound;
	public AudioClip playerExplSound;
	public AudioClip enemyDamage;
	public int typeOfEnemy;
	public float normalScale;
	public float bigScale;
	public TrailRenderer trail;
	public Particle particles;
	public Color[] colorType;

	private float enemySpeed;
	private GameObject scoreGUI;
	private int life = 1;
	private Color particleColor = Color.white;
	private float baseSpeed;
	private float fxVolume;
	private int fxMute;

	void SetSpeed (float speed) {
		enemySpeed = speed;
		baseSpeed = speed;
	}

	void SetType (int type){
		typeOfEnemy = type;
	}

	void Start () {
		fxVolume = PlayerPrefs.GetFloat("fxVolume")*0.9f;
		fxMute = PlayerPrefs.GetInt("fxMute");
		scoreGUI = GameObject.Find("ScoreGUI");

		if (typeOfEnemy == 2)
			life = 2;
	}

	void Update () {
		//Mirar al centro del mapa
		transform.LookAt(new Vector3(0f,0f,-10f), Vector3.back);
		transform.eulerAngles = new Vector3(0,0,-transform.eulerAngles.z);

		//Moverse en el eje -y
		if (typeOfEnemy != 1)
			transform.Translate(Vector3.down * Time.deltaTime*enemySpeed);

		if (typeOfEnemy == 1){
			transform.Translate(Vector3.down * Time.deltaTime*enemySpeed * 0.7f);
			transform.Translate(Vector3.right * Time.deltaTime*enemySpeed*3f);
			gameObject.SendMessage("SetColor",colorType[0]);
		}

		if (typeOfEnemy == 2){
			if (life >= 2){
				transform.localScale = new Vector3(bigScale,bigScale,1);
				trail.startWidth = bigScale/100;
				trail.endWidth = bigScale/100;
				gameObject.SendMessage("SetColor",colorType[1]);

				enemySpeed = baseSpeed/2;
			}
			if (life == 1){
				transform.localScale = new Vector3(normalScale,normalScale,1f);
				trail.startWidth = normalScale/100;
				trail.endWidth = normalScale/100;
				gameObject.SendMessage("SetColor",Color.white);

				enemySpeed = baseSpeed;
			}
		}
		if (life == 0)
			Die ();
	}

	void SetColor (Color actualColor) {
		GetComponent<SpriteRenderer>().color = actualColor;
		trail.material.color = actualColor;
		particleColor = actualColor;
	}

	void Touched () {
		if (life == 2){
			if (fxMute == 0)
				GameObject.Find("Scene").audio.PlayOneShot(enemyDamage, fxVolume);

			Transform explosionParticles;
			explosionParticles = Instantiate(explosionPrefab,transform.position,Quaternion.identity) as Transform;
			Destroy (explosionParticles.gameObject,1f);
			explosionParticles.particleSystem.startColor = particleColor;
		}
		life--;
	}

	void Die () {
		Destroy (gameObject);
		Transform explosionParticles;
		explosionParticles = Instantiate(explosionPrefab,transform.position,Quaternion.identity) as Transform;
		Destroy (explosionParticles.gameObject,1f);
		explosionParticles.particleSystem.startColor = particleColor;

		if (fxMute == 0)
			GameObject.Find("Scene").audio.PlayOneShot(enemyExplSound, fxVolume);

		scoreGUI.transform.SendMessage("AddScore");
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.transform.tag == "Player"){
			Destroy (gameObject);

			GameObject.Find("Scene").BroadcastMessage("Lose");
			if (fxMute == 0)
				GameObject.Find("Scene").audio.PlayOneShot(playerExplSound, fxVolume);
		}
		else {
			Physics2D.IgnoreCollision(coll.gameObject.collider2D, collider2D);
		}
	}
}