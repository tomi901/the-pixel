﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemySpawner : MonoBehaviour {
	public Transform enemyPrefab;
	public float startTime;
	public float minWaitTime;
	public float maxWaitTime;
	public float minSpeed;
	public float maxSpeed;
	public float spawnRange;
	public float minSeparation;
	public float difficultyMultiplier;
	public float speedDifficultyMultiplier;
	public GameObject startCounterGUI;
	public Text scoreGUI;
	public float[] chancesOfType;
	public float[] scoreToSpawnType;

	private float timer;
	private float waitTime;
	private float lastAngle;
	private float angle;
	private float difficulty;
	private float startDifficulty;
	private float speed;
	private GameObject counterObject;
	private float maxChance;
	private float leftChance;
	private int actualScore;

	void Start () {
		timer = Time.timeSinceLevelLoad + startTime;
		waitTime = Random.Range (minWaitTime,maxWaitTime);
		angle = Random.Range (0,360);
		startDifficulty = 1-startTime*difficultyMultiplier;

		startCounterGUI.GetComponent<Text>().enabled = true;
		scoreGUI.enabled = false;
		counterObject = GameObject.Find("StartCounter");

		for(var i = 0 ; i < chancesOfType.Length ; i ++)
			maxChance += chancesOfType[i];
	}

	void Score (int score) {
		actualScore = score;
	}

	void Update () {
		leftChance = maxChance;

		int startCounter;
		startCounter = Mathf.CeilToInt(startTime-Time.timeSinceLevelLoad);

		if (startCounter>0) {
			startCounterGUI.GetComponent<Text>().text = startCounter.ToString();
		}
		else {
			GameObject.Destroy(counterObject);
			scoreGUI.enabled = true;
		}

		if (timer < Time.timeSinceLevelLoad) {
			//angulo random
			float actualAngle = angle*Mathf.Deg2Rad;
			angle += Random.Range (minSeparation,360-minSeparation);
			if (angle > 360)
				angle -= 360;

			//spawnear enemigo
			Transform enemy;
			enemy = Instantiate(enemyPrefab, new Vector3(Mathf.Cos(actualAngle), Mathf.Sin(actualAngle), 0)*spawnRange, Quaternion.identity) as Transform;

			//aumentar dificultad
			difficulty = startDifficulty + Time.timeSinceLevelLoad*difficultyMultiplier;
			
			//tiempo random para el siguiente enemigo
			timer = Time.timeSinceLevelLoad + waitTime;
			waitTime = Random.Range(minWaitTime,maxWaitTime)/difficulty;

			//velocidad del enemigo, aumenta con la dificultad(tiempo)
			float speedDifficulty = (difficulty-1)*speedDifficultyMultiplier+1;
			speed = Random.Range(minSpeed,maxSpeed)*speedDifficulty;
			enemy.SendMessage("SetSpeed",speed,SendMessageOptions.DontRequireReceiver);

			//posibilidad de spawnear diferentes tipos de enmigo
			float chance = Random.Range(0,100);
			if (actualScore > scoreToSpawnType[0]){
				if (chance >= leftChance - chancesOfType[1]){
					enemy.SendMessage("SetType",1,SendMessageOptions.DontRequireReceiver);
				}
			}
			leftChance -= chancesOfType[1];
			if (actualScore > scoreToSpawnType[1]){
				if (chance >= leftChance - chancesOfType[2] && chance < leftChance){
					enemy.SendMessage("SetType",2,SendMessageOptions.DontRequireReceiver);
				}
			}
		}
	}

	void Lose () {
		gameObject.GetComponent<EnemySpawner>().enabled = false;
	}
}