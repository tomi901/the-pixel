﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoseGame : MonoBehaviour {
	public Transform explosionPrefab;
	public Transform player;
	public Font arcadeFont;
	public string[] text;
	public ScoreGUI scoreGUI;
	public SelectColor colorScript;
	public Transform loseCanvas;
	public Transform recordText;
	public Text lastRecord;

	public admobScript admob;
	public float timesToShowInterstitial;

	private GameObject[] enemies;
	private bool lose;
	private int highScore;
	private bool ifRecord;

	void Start () {
		highScore = PlayerPrefs.GetInt("record");
		lastRecord.text = "Last record:  "+PlayerPrefs.GetInt("record");

		PlayerPrefs.SetInt("TimesToShow",PlayerPrefs.GetInt("TimesToShow")+1);
		PlayerPrefs.Save();
	}

	void Lose () {
		//Destruir jugador
		Destroy(player.gameObject);
		Transform explosionParticles;
		explosionParticles = Instantiate(explosionPrefab,player.transform.position,Quaternion.identity) as Transform;
		explosionParticles.particleSystem.startColor = colorScript.colors[PlayerPrefs.GetInt("colorInt")];
		Destroy (explosionParticles.gameObject,2f);

		//Destruir enemigos
		enemies = GameObject.FindGameObjectsWithTag ("Enemy");
		for(var i = 0 ; i < enemies.Length ; i ++)
			Destroy(enemies[i]);

		//High score
		int actualScore = scoreGUI.score;
		if (actualScore>highScore)
			ifRecord = true;

		highScore = Mathf.Max(actualScore,highScore);
		PlayerPrefs.SetInt("record", highScore);
		PlayerPrefs.Save();

		lose = true;
	}

	void Update () {
		if (PlayerPrefs.GetInt("TimesToShow") >= timesToShowInterstitial && lose){
			admob.ShowInterstitial();
			PlayerPrefs.SetInt("TimesToShow",0);
			PlayerPrefs.Save();
		}
		loseCanvas.gameObject.SetActive(lose);
		recordText.gameObject.SetActive(ifRecord);
	}

	void TryAgain () {
		Application.LoadLevel(1);
	}
	void MainMenu () {
		Application.LoadLevel(0);
	}
}