﻿using UnityEngine;
using System.Collections;

public class TouchInput : MonoBehaviour {
	void Update () {
		/**
		if (Input.touchCount > 0 && (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)){
			OnTouch();
		}
		if (Application.platform == RuntimePlatform.WindowsEditor && Input.GetMouseButtonDown(0)) {
			OnTouch();
		}
		OnTouch();
		*/
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
			if (Input.touchCount > 0) {
				Touch touch = Input.GetTouch(0);
				if (touch.phase == TouchPhase.Began)
					OnTouch(Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touch.position),Vector2.zero));
			}
		}
		else {
			if (Input.GetMouseButtonDown(0))
				OnTouch(Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition),Vector2.zero));
		}
	}

	void OnTouch (RaycastHit2D hit) {
		if (hit) {
			if (hit.transform.tag == "Enemy") {
				hit.transform.SendMessage("Touched");
			}
		}
	}
}
