﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MusicSelector : MonoBehaviour {
	public AudioClip[] music;
	public string[] musicName;
	public string[] author;
	public Transform MusicObjectName;
	public Text AuthorNameText;
	public float nameShowTime;
	public float speed;

	private Text MusicTextName;
	private float yPos;
	private bool hiding;
	private float musicVolume;
	private int musicMute;

	void Start () {
		musicVolume = PlayerPrefs.GetFloat("musicVolume");
		musicMute = PlayerPrefs.GetInt("musicMute");

		int musicSelector = Random.Range(0,music.Length);

		if (!audio.playOnAwake && musicMute == 0) {
			audio.clip = music[musicSelector];
			audio.loop = true;
			audio.volume = musicVolume;
			if(!audio.isPlaying){ 
				audio.Play();
			}
		}
		MusicTextName = MusicObjectName.GetComponent<Text>();

		MusicTextName.resizeTextForBestFit = false;
		AuthorNameText.resizeTextForBestFit = false;

		MusicTextName.text = musicName[musicSelector];
		AuthorNameText.text = author[musicSelector];
	}

	IEnumerator ShowName () {
		if (!hiding)
			yPos = Mathf.Lerp (-25f,10f,Time.timeSinceLevelLoad*speed);
		yield return new WaitForSeconds(nameShowTime);
		hiding = true;
		yPos = Mathf.Lerp (10f,-25f,(Time.timeSinceLevelLoad-nameShowTime)*speed);
	}

	void Update () {
		StartCoroutine ("ShowName");
		MusicObjectName.position = new Vector3(MusicObjectName.position.x,yPos,0f);
	}

	void Lose () {
		audio.volume = 0.5f;
	}
}
