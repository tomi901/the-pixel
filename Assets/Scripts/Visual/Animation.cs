﻿using UnityEngine;
using System.Collections;

public class Animation : MonoBehaviour {
	public float rotSpeed = 60f;
	public float scaleSpeed = 0.05f;
	public float maxScale = 0.02f;

	private float scale;
	private float originalScale;

	void Start () {
		originalScale = transform.localScale.x;
	}

	void Update () {
		transform.Rotate(Vector3.forward * Time.deltaTime * rotSpeed, Space.World);
		scale = originalScale+Mathf.PingPong(Time.time*scaleSpeed, maxScale-originalScale);
		transform.localScale = new Vector3(scale,scale,0);
	}
}
