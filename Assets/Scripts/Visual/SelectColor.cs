﻿using UnityEngine;
using System.Collections;

public class SelectColor : MonoBehaviour {
	public int selectedColor;
	public Color[] colors;
	public GameObject[] items;

	private Color color;

	void Start () {
		selectedColor = PlayerPrefs.GetInt("colorInt");

		color = colors[selectedColor];
	}

	void Update () {
		items[0].GetComponent<SpriteRenderer>().color = color;
		items[1].GetComponent<SpriteRenderer>().color = color;
		items[2].GetComponent<ParticleSystem>().startColor = colors[selectedColor];
	}
}